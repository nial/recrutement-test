# -*- coding: utf-8 -*-

from odoo import models, fields, api, exceptions


class Produits(models.Model):
    _inherit = 'product.product'

    type_produit = fields.Selection([('fruit', 'Fruit'), ('legume', 'Légume')], string="Nature du produit", default="fruit")
    date_recolte = fields.Date(string="Date de la recolte", default=fields.datetime.now())
    state = fields.Selection([("frais", "Frais"),("recent", "Recent"),("fletri", "Fletri"), ("poubelle", "Poubelle")], 
              string="Etat", default="frais")

    @api.multi
    @api.constrains('date_recolte')
    def date_recolte_constrains(self):
        for rec in self:
            if fields.Datetime.from_string(rec.date_recolte) > fields.datetime.now():
                raise exceptions.ValidationError("La date de recolte est superieure a la date actuelle")
    
    @api.onchange('date_recolte')
    def onchange_date_recolte(self):
        self.ensure_one()
        if self.date_recolte:
            temp = fields.datetime.now() - fields.Datetime.from_string(self.date_recolte)
            if temp.days == 0:
                self.state = "frais"
            elif temp.days == 1:
                self.state = "recent"
            elif temp.days >=3 and temp.days <= 7:
                self.state  = "fletri"
            else:
                self.state = "poubelle"